difference() {
translate([0,170,0])
rotate([90,0,0])
linear_extrude(height=170) {
		polygon(points=[[0,0],[0,11],[3,11.5],[4,9.5],[2,9.5],[2,2],[7.5,2],[7.5,7],[6.5,7.5],[9,9],[9,3],[10,2],[76,2],[77,3],[77,9],[80,7.5],[79,6.5],[79,2],[85,2],[85,9.5],[82,9.5],[83,11.5],[86,11],[86,0]

], paths=[[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23]]);
}
// cap holes
translate([32.075,10.16,-0.1])
cylinder(h=9,r=5.5,$fn=24);
translate([55.57,10.16,-0.1])
cylinder(h=9,r=5.5,$fn=24);

translate([32.075,27.94,-0.1])
cylinder(h=9,r=5.5,$fn=24);
translate([55.57,27.94,-0.1])
cylinder(h=9,r=5.5,$fn=24);

translate([32.075,45.72,-0.1])
cylinder(h=9,r=5.5,$fn=24);
translate([55.57,45.72,-0.1])
cylinder(h=9,r=5.5,$fn=24);

translate([32.075,63.5,-0.1])
cylinder(h=9,r=5.5,$fn=24);
translate([55.57,63.5,-0.1])
cylinder(h=9,r=5.5,$fn=24);

translate([32.075,81.28,-0.1])
cylinder(h=9,r=5.5,$fn=24);
translate([55.57,81.28,-0.1])
cylinder(h=9,r=5.5,$fn=24);

translate([32.075,99.06,-0.1])
cylinder(h=9,r=5.5,$fn=24);
translate([55.57,99.06,-0.1])
cylinder(h=9,r=5.5,$fn=24);

translate([32.075,116.84,-0.1])
cylinder(h=9,r=5.5,$fn=24);
translate([55.57,116.84,-0.1])
cylinder(h=9,r=5.5,$fn=24);

translate([32.075,134.62,-0.1])
cylinder(h=9,r=5.5,$fn=24);
translate([55.57,134.62,-0.1])
cylinder(h=9,r=5.5,$fn=24);

}

// standoffs
translate([23,11,0.1])
difference() {
	cylinder(h=9,r=2.5,$fn=24);
	translate([0,0,-0.5]) 	cylinder(h=11,r=1.3,$fn=24);
}
translate([64,11,0.1])
difference() {
	cylinder(h=9,r=2.5,$fn=24);
	translate([0,0,-0.5]) 	cylinder(h=11,r=1.3,$fn=24);
}
translate([23,61,0.1])
difference() {
	cylinder(h=9,r=2.5,$fn=24);
	translate([0,0,-0.5]) 	cylinder(h=11,r=1.3,$fn=24);
}
translate([64,61,0.1])
difference() {
	cylinder(h=9,r=2.5,$fn=24);
	translate([0,0,-0.5]) 	cylinder(h=11,r=1.3,$fn=24);
}
translate([23,111,0.1])
difference() {
	cylinder(h=9,r=2.5,$fn=24);
	translate([0,0,-0.5]) 	cylinder(h=11,r=1.3,$fn=24);
}
translate([64,111,0.1])
difference() {
	cylinder(h=9,r=2.5,$fn=24);
	translate([0,0,-0.5]) 	cylinder(h=11,r=1.3,$fn=24);
}
translate([23,161,0.1])
difference() {
	cylinder(h=9,r=2.5,$fn=24);
	translate([0,0,-0.5]) 	cylinder(h=11,r=1.3,$fn=24);
}
translate([64,161,0.1])
difference() {
	cylinder(h=9,r=2.5,$fn=24);
	translate([0,0,-0.5]) 	cylinder(h=11,r=1.3,$fn=24);
}
