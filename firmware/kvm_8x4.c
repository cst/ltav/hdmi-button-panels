#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>
#include <string.h>
#include "usart2.c"


#include "config.h"
#include <util/delay.h>
#include <util/delay_basic.h>

// WEST 1
// EAST 0
//#define bench 0

/* 8x4 switch matrix front end, for Extron
    By default these switches are set to 9600 baud (good) and ID 1 (we use 0, so needs to be changed)

    Switch inputs on Ports B (projector) and C (LCD)
    LED outputs driven through a 74LS138 on PORTA (Projector) and PORTD (LCD) where C = PD7, B=BD6, A=PD5, Enable = PD4
    Uses RS232 at 9600 baud

    Change IO protocol is the string <in>*<out>!
    where <in> and <out> are single digits, counting from 1

    Switch debounce is carried out by having an 8 bit timer (timer0) generating interrupts
    every 2mS.  When any switch has been down for a count of 16, then the inputs from
    switches are sampled, and any which are down at that time are processed. If more than 1
    switch in a row is down, the leftmost switch has priority. Once a switch has been detected
    as down, a string is sent to the matrix switch, and that particular switch locked out
    until a 500mS timer has expired. This prevents swamping the matrix switch with RS232 traffic.

    To cope with two simultaneous switch changes, RS232 commands are buffered, and sent to the switch
    at 9600 baud. A wait character * is inserted into the buffer (but not the RS232 stream sent to the
    video matrix switch) to cause a 10mS delay, to give the matrix switch time to process the change.

    The 2mS interrupt also clears the watchdog counter. If the timer stops, the watchdog will
    reset the microcontroller after approx 500mS

    The same function which appends the RS232 string to the buffer to be sent to the matrix switch also
    sets and clears relevant LEDs on the front panel, so providing positive feedback.

    Initial state is that both outputs are connected to input 1, this is both the matrix switch
    default, and the initial state of the LEDs.

    LT1 and LT2 have different blank behaviour:
        LT1, on select blank, the previously selected input is shown dimmed, and pressing blank again releases the blank
        LT2, blank is just like selecting an all-blank input.
*/


volatile uint8_t switch_debounce_top[8] ;
volatile uint8_t switch_debounce_bottom[8] ;
volatile uint8_t switch_lockout_top ;
volatile uint8_t switch_lockout_bottom ;

volatile uint8_t blanked ;                  //  values non zero = blanked
volatile uint8_t led_state_top ;            //  values 0..7 = which led to light
volatile uint8_t led_state_bottom ;         //  values 0..7 = which led to light
volatile uint8_t two_mscount ;

/* West column selections expected to be (button numbers from schematic, SW8 = leftmost) :
top row button numbers (NOT Extron input nos)
8   Visualiser
7   HDMI
6   VGA
5   -
4   -
3   Other bench
2   Blank
1   -

bottom row button numbers (NOT Extron input nos)
8   Visualiser
7   HDMI
6   VGA
5   -
4   -
3   Other bench
2   -
1   -


/* East column selections expected to be:
top row button numbers (NOT Extron input nos)
8   Visualiser
7   HDMI
6   VGA
5   PC0
4   PC1
3   Other bench
2   Blank
1   -

bottom row button numbers (NOT Extron input nos)
8   Visualiser
7   HDMI
6   VGA
5   PC0
4   PC1
3   Other bench
2   -
1   -



For EDID see Extron manual page 65

*/

// Top row      - Assign Extron input n [in range 1..8] to output 1(projector) and output3 (other bench) and output 4 (recorder)
// Bottom row   - Assign Extron input n [in range 1..8] to output 2(bench screen)


// Change on 9/11/2016. We want button 5 (PC1) to select the audio from PC0

/*
// East bench LT1 (blank is pos7)
uint8_t switchstrings [17][20] = {
"\x1f+Q8*1!8*3!8*4!\r",         // Out1 <- in8, Out3 <- in8, Out4 <- in8  NO BUTTON FITTED
"1*1B",                         // blank projector
"\x1f+Q6*1!6*3!6*4!\r",         // Out1 <- in6, Out3 <- in6, Out4 <- in6  West bench
"5*1%4*1$5*3!5*4!\r",       // Out1 <- in5 (video only), Out1 <- in4 (audio only), Out3 <- in5, Out4 <- in5  PC1
"\x1f+Q4*1!4*3!4*4!\r",         // Out1 <- in4, Out3 <- in4, Out4 <- in4  PC0
"\x1f+Q3*1!3*3!3*4!\r",         // Out1 <- in3, Out3 <- in3, Out4 <- in3  VGA
"\x1f+Q2*1!2*3!2*4!\r",         // Out1 <- in2, Out3 <- in2, Out4 <- in2  HDMI
"\x1f+Q1*1!1*3!1*4!\r",         // Out1 <- in1, Out3 <- in1, Out4 <- in1  Vis
"8*2!",                         // Out2 <- in8  NO BUTTON FITTED
"7*2!",                         // Out2 <- in7  NO BUTTON FITTED
"6*2!",                         // Out2 <- in6
"5*2!",                         // Out2 <- in5
"4*2!",                         // Out2 <- in4
"3*2!",                         // Out2 <- in3
"2*2!",                         // Out2 <- in2
"1*2!",                         // Out2 <- in1
"1*0B"                          // unblank projector
};

*/

// East bench LT2 (blank is pos8)
uint8_t switchstrings [17][20] = {
"1*1B",                         // blank projector
"\x1f+Q7*1!7*3!7*4!\r",         // Out1 <- in7, Out3 <- in8, Out4 <- in8  NO BUTTON FITTED
"\x1f+Q6*1!6*3!6*4!\r",         // Out1 <- in6, Out3 <- in6, Out4 <- in6  West bench
"5*1%4*1$5*3!5*4!\r",       // Out1 <- in5 (video only), Out1 <- in4 (audio only), Out3 <- in5, Out4 <- in5  PC1
"\x1f+Q4*1!4*3!4*4!\r",         // Out1 <- in4, Out3 <- in4, Out4 <- in4  PC0
"\x1f+Q3*1!3*3!3*4!\r",         // Out1 <- in3, Out3 <- in3, Out4 <- in3  VGA
"\x1f+Q2*1!2*3!2*4!\r",         // Out1 <- in2, Out3 <- in2, Out4 <- in2  HDMI
"\x1f+Q1*1!1*3!1*4!\r",         // Out1 <- in1, Out3 <- in1, Out4 <- in1  Vis
"8*2!",                         // Out2 <- in8  NO BUTTON FITTED
"7*2!",                         // Out2 <- in7  NO BUTTON FITTED
"6*2!",                         // Out2 <- in6
"5*2!",                         // Out2 <- in5
"4*2!",                         // Out2 <- in4
"3*2!",                         // Out2 <- in3
"2*2!",                         // Out2 <- in2
"1*2!",                         // Out2 <- in1
"1*0B"                          // unblank projector
};



/*
// West bench
uint8_t switchstrings [17][20] = {
"",
"1*1B",             // blank projector
"",
"\x1f+Q6*1!6*3!\r",
"\x1f+Q3*1!3*3!\r",
"\x1f+Q2*1!2*3!\r",
"\x1f+Q1*1!1*3!\r",
"",

"",
"",
"",
"6*2!",
"3*2!",
"2*2!",
"1*2!",
"",
"1*0B"                          // unblank projector
};

*/




uint8_t buffer[256] ;                                           // To cope with two switch changes at once, the RS232 commands are appended to
                                                                // a buffer if there is room. If the buffer is full, extra commands are discarded.
uint8_t buf_r_ptr ;                                             // Position in buffer of next character to be written
uint8_t buf_w_ptr ;                                             // Position in buffer of next character to be read



void port_direction_init() {

    DDRA = 0xF0;                                                // 138 drive
    DDRB = 0;                                                   // all inputs
    DDRC = 0;                                                   // all inputs
    DDRD = 0xF2;                                                // 138 drive, plus TxD

    PORTA = 0xF0 ;                                              // LED7 (visualiser) on top row
    PORTB = 0xFF ;                                              // pull up all switch inputs on top row
    PORTC = 0xFF ;                                              // pull up all switch inputs on bottom row
    PORTD = 0xF0 ;                                              // LED7 (visualiser) on bottom row
}


void timers_init() {                                            // Set up timers, both normal mode, both interrupt on overflow.
                                                                    // Timer 1 stopped initially, started with TCCR1B = 7 gives approx 500mS timeout
    TCCR0A = 0;
    TCCR0B = 0x02 ;                                                 // 8MHz / 64 clock rate
    TIMSK0 = 0x01 ;                                                 // interrupt on overflow, = 2mS

    TCCR1A = 0;
    TCCR1B = 0 ;                                                    // stopped
    TCCR1C = 0 ;                                                    // stopped
    TIMSK1 = 0x01 ;                                                 // interrupt on overflow
}


// Append an RS232 command to buffer
void append_buf_string(uint8_t in) {
    uint8_t l ;
    l = strlcpy(&buffer[buf_w_ptr],switchstrings[in],32) ;
    buf_w_ptr += l ;        // Buffer is 256 bytes, so wraps correctly.
}



//  switch_lockout_top | switch_lockout_bottom = 1  blocks swamping the switch. Setting all bits to 0 permits processing switch changes.
ISR(TIMER1_OVF_vect) {
    switch_lockout_top = 0 ;                            // 1 = lockout subsequent switch changes
    switch_lockout_bottom = 0 ;                         // 1 = lockout subsequent switch changes
    TCCR1B = 0 ;                                        // stop timer 1
}


// 2mS interrupt
ISR(TIMER0_OVF_vect) {

    uint8_t i ;

    wdt_reset();

    two_mscount++;
//  two_mscount = two_mscount & 0x1F ;

    PORTA &= 0x0F;
    PORTD &= 0x0F;
    if (blanked) {
        if ((two_mscount & 0x3F) > 1) {
// West
//          PORTA |= ((1<<5) + 0x10);                   // Illuminate blank + 0x10 is enable
// East, LT1
//          PORTA |= 0x30;                              // Illuminate blank + 0x10 is enable
// East, LT2
            PORTA |= 0x10;                              // 0x10 is enable
        } else {
            PORTA |= ((led_state_top<<5) + 0x10);       // illuminate previous state
        }
    } else {
        PORTA |= ((led_state_top<<5) + 0x10);
    }
    PORTD |= ((led_state_bottom<<5) + 0x10);


// Get switch pressed state
    for (i=0;i<8;i++) {
        switch_debounce_top[i] = switch_debounce_top[i]<<1 ;
        if ((PINB >> i) & 0x01) {
            switch_debounce_top[i] |= 0x01 ;            // Switch is up, set to 1
        } else {
            switch_debounce_top[i] &= 0xFE ;            // Set to 0 if switch is down. Whole Byte=0 means switch is accepted as being down
        }
        switch_debounce_bottom[i] = switch_debounce_bottom[i]<<1 ;
        if ((PINC >> i) & 0x01) {
            switch_debounce_bottom[i] |= 0x01 ;
        } else {
            switch_debounce_bottom[i] &= 0xFE ;
        }
    }



// 8 consecutive down in 2mS is accepted as switch down.
    if (switch_lockout_top == 0) {
// West
//      for (i=1;i<7;i++) {
// East LT1
//      for (i=1;i<8;i++) {
// East LT2
        for (i=0;i<8;i++) {
            if (switch_debounce_top[i] == 0) {
                switch_lockout_top = 1 ;
                TCCR1B = 0x03;
                TCNT1 = 0 ;
                if (i == 1) {                           // blank button.   9/11/2016 this was set to 0
                    if (blanked) {
                        append_buf_string(16);              // unblank
                        blanked = 0 ;
                    } else {
                        append_buf_string(1) ;              // blank
                        blanked = 1 ;
                    }
                } else {
                    blanked = 0 ;
                    append_buf_string(16);              // send unblank
                    append_buf_string(i);
                    led_state_top = i ;
                }
            }
        }
    }


// 8 consecutive down in 2mS is accepted as switch down.
    if (switch_lockout_bottom == 0) {
// West
//      for (i=1;i<7;i++) {
// East LT1
//      for (i=1;i<8;i++) {
// East LT2
        for (i=0;i<8;i++) {
            if (switch_debounce_bottom[i] == 0) {
                switch_lockout_bottom = 1 ;
                TCCR1B = 0x03;
                TCNT1 = 0 ;
                append_buf_string(i+8);
                led_state_bottom = i ;
            }
        }
    }
}




int main(void) {

    port_direction_init() ;
    USART0_init(51);                                // 9600 at 8MHz
    PRR = 0 ;
    wdt_reset();
    wdt_enable(9);
    timers_init();
    SREG |= 0x80 ;                              // Turn on interrupts

    buf_w_ptr = 0 ;
    buf_r_ptr = 0 ;

// Initial State
//  USART0_transmit_string(reset);

// West
//  append_buf_string(6);
//  PORTA = 0xDF;
//  append_buf_string(14);
//  PORTD = 0xDF;

// East
    append_buf_string(7);                   // strings number from 0
    led_state_bottom = 7;
    append_buf_string(15);
    led_state_top = 7;



// We build strings in buffer, pointed to by buf_w_ptr
// If the read pointer buf_r_ptr is less, then we have data to send.
// If the character is =, then delay by 40mS instead of sending to give the switch time to notice the change

    while(1) {
        if (buf_w_ptr != buf_r_ptr) {
            if (buffer[buf_r_ptr] != '=') {
                USART0_transmit(buffer[buf_r_ptr]);
            } else {
                _delay_ms(40);
            }
            buf_r_ptr++;                    // 256 byte buffer so wraps naturally
        }
        _delay_us(10);
    }
}

