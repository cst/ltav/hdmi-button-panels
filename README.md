[Lecture theatres LT1 and
LT2](https://www.cl.cam.ac.uk/local/sys/ltav/) of the William Gates
Building have presentation benches with built-in HDMI switches.
Bench-top push-button panels allow speakers to control the video
routing from several HDMI inputs to projectors and bench-top displays.

This directory contains the design information for these four
push-button panels, which were designed and manufactured by Brian
Jones (around 2010).

Behind each of these panels is a PCB with an [Atmel/Microchip
megaAVR](https://ww1.microchip.com/downloads/en/DeviceDoc/ATmega164P-324P-644P-Data-Sheet-40002071A.pdf)
8-bit microcontroller that reads the buttons, controls the LED
indicators, and sends serial-port commands to the [Extron DXP 84 HDMI
switch](https://www.extron.com/product/dxphdmi) in the respective
bench, to program its switch matrix.

The microcontroller shown in the [schematic](circuit/LT8x4.pdf) is the
[Atmel ATmega664A](https://www.microchip.com/en-us/product/ATmega644A)
(64 Kbyte Flash, 2 Kbyte EEPROM, 4 Kbyte RAM), but (at least some of)
the actual boards use the smaller/older version [Atmel
ATmega164P](https://www.microchip.com/en-us/product/ATmega164P) (16
Kbyte Flash, 512 Kbyte EEPROM, 1 Kbyte RAM).

This directory contains the

* GCC C source code for firmware,
* Eagle files for the schematics and the PCB layout,
* [OpenSCAD](https://openscad.org/) sources for 3D-printed plastic parts.

Linux (e.g. Ubuntu 20.04) packages required for compiling and
installing the firmware:

```
# apt-get install gcc-avr avrdude avr-libc
```

The PCB has a 5-pin in-system programming interface
(GND-SCK-Miso-Mosi-Reset) designed to the match a [Tuxgraphics
programmer](http://tuxgraphics.org/electronics/200901/tuxgraphics-isp-header.shtml).
Atmel’s [AVR
Dragon](https://www.microchip.com/en-us/development-tool/ATAVRDRAGON)
instead uses 6 pins in a 3x2 matrix, 5 signals match the Tuxgraphics
ones, the extra one is a Vcc supply.

![Photo of the front of the LT1 east panel](panel-front.jpg)

![Photo of the back of the LT2 east panel](panel-back.jpg)
